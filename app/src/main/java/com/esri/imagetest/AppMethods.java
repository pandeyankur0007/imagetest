package com.esri.imagetest;

import android.app.DatePickerDialog;
import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.core.location.LocationManagerCompat;


import java.nio.charset.StandardCharsets;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AppMethods {

    public static void shortToast(String msg, Context context) {
        Toast.makeText(context, Html.fromHtml("<font color='#ffffff' ><b>" + msg + "</b></font>"), Toast.LENGTH_SHORT).show();
    }

    public static void longToast(String msg, Context context) {
        Toast.makeText(context, Html.fromHtml("<font color='#ffffff' ><b>" + msg + "</b></font>"), Toast.LENGTH_LONG).show();
    }

    public static void hideKeyboard(View view, Context context) {
        if (context != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null) {
                if (view != null) {
                    inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }
    }

    public static boolean isDeviceOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectivityManager != null) {
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return networkInfo != null && networkInfo.isConnected();
    }

    public static String getErrorMessage(int code) {
        String message;
        switch (code) {
            case 401:
                message = "Entered Username & Password is wrong.";
                break;
            case 404:
                message = "Something went Wrong, Please check your server setting.";
                break;
            case 403:
                message = "The server understood the request, but is refusing to fulfill it.";
                break;
            case 500:
                message = "Internal Server Error, Please try after sometime.";
                break;
            default:
                message = "Internal Server Error, Please try again later.";
                break;
        }
        return message;
    }

    public static String convertDateToString(Date date) {
        Format formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        return formatter.format(date);
    }

    public static String convertLongDateIntoString(Long longDate) {
        if (longDate != null) {
            return DateFormat.format("dd-MM-yyyy", new Date(longDate)).toString();
        } else {
            return null;
        }
    }

    public static Date convertStringDateToDate(String dateString) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            return formatter.parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void datePicker(final Context context, EditText editTextDat) {

        Calendar c = Calendar.getInstance();
        final int[] mYear = {c.get(Calendar.YEAR)};
        final int[] mMonth = {c.get(Calendar.MONTH)};
        final int[] mDay = {c.get(Calendar.DAY_OF_MONTH)};

        final EditText editTextDate = editTextDat;
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, (view, year, monthOfYear, dayOfMonth) -> {

            mYear[0] = year;
            mMonth[0] = monthOfYear;
            mDay[0] = dayOfMonth;

            if (view.isShown()) {
                if (mDay[0] <= 9 && (mMonth[0] + 1) <= 9) {
                    editTextDate.setText("0" + mDay[0] + "-0" + (mMonth[0] + 1) + "-" + mYear[0]);//dd-MM-yyyy
                } else if (mDay[0] <= 9) {
                    editTextDate.setText("0" + mDay[0] + "-" + (mMonth[0] + 1) + "-" + mYear[0]);
                } else if ((mMonth[0] + 1) <= 9) {
                    editTextDate.setText(mDay[0] + "-0" + (mMonth[0] + 1) + "-" + mYear[0]);
                } else {
                    editTextDate.setText(mDay[0] + "-" + (mMonth[0] + 1) + "-" + mYear[0]);
                }
            }
        }, mYear[0], mMonth[0], mDay[0]);
        datePickerDialog.show();
    }

    //float formatter
    public static float floatFormatter(String value) {
        float floatValue;
        if (TextUtils.isEmpty(value)) {
            return 0;
        }
        try {
            floatValue = Float.parseFloat(value);
        } catch (NumberFormatException ex) {
            floatValue = 0f;
        }
        return floatValue;
    }

    //double formatter
    public static double doubleFormatter(String value) {
        double doubleValue;
        if (TextUtils.isEmpty(value)) {
            return 0;
        }
        try {
            doubleValue = Double.parseDouble(value);
        } catch (NumberFormatException ex) {
            doubleValue = 0;
        }
        return doubleValue;
    }

    public static int integerFormatter(String value) {
        int intValue;
        if (TextUtils.isEmpty(value)) {
            return 0;
        }
        try {
            intValue = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            intValue = 0;
        }
        return intValue;
    }

    public static Boolean isLocationEnabled(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {

            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if (locationManager != null) {
                return LocationManagerCompat.isLocationEnabled(locationManager);
            } else {
                return false;
            }
        } else {

            int mode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE, Settings.Secure.LOCATION_MODE_OFF);
            return (mode != Settings.Secure.LOCATION_MODE_OFF);
        }
    }

    public static String base64Encode(String token, String userName) {
        String newToken = userName.concat(":").concat(token);
        byte[] encodedBytes = Base64.encode(newToken.getBytes(), Base64.NO_WRAP);
        return new String(encodedBytes, StandardCharsets.UTF_8);
    }
}