package com.esri.imagetest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.developer.filepicker.controller.DialogSelectionListener;
import com.developer.filepicker.model.DialogConfigs;
import com.developer.filepicker.model.DialogProperties;
import com.developer.filepicker.view.FilePickerDialog;
import com.esri.imagetest.databinding.ActivityMainBinding;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private File demoFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        AppPermissions.allPermission(this);
        registerListener();
    }

    private void registerListener() {
        binding.buttonAddFile.setOnClickListener(view -> {
            DialogProperties properties = new DialogProperties();
            properties.selection_mode = DialogConfigs.SINGLE_MODE;
            properties.selection_type = DialogConfigs.FILE_SELECT;
            properties.root = new File(DialogConfigs.DEFAULT_DIR);
            properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
            properties.offset = new File(DialogConfigs.DEFAULT_DIR);
            properties.extensions = null;
            properties.show_hidden_files = false;

            FilePickerDialog dialog = new FilePickerDialog(MainActivity.this, properties);
            dialog.setTitle("Select a File");

            dialog.setDialogSelectionListener(files -> {
                //files is the array of the paths of files selected by the Application User.
                if (!TextUtils.isEmpty(files[0])) {
                    binding.tvAddFile.setText(String.format("%s", files[0]));
                    demoFile = new File(files[0]);
                }
            });

            dialog.show();
        });

        binding.buttonUploadFile.setOnClickListener(view -> {
            if (demoFile != null) {
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("file", "test_demo",
                                RequestBody.create(MediaType.parse("application/octet-stream"),
                                        demoFile))
                        .build();
                Request request = new Request.Builder()
                        .url("https://sbmgateway.sbmurban.org/api/v1/media/upload")
                        .method("POST", body)
                        .addHeader("Authorization", "Basic bm9kYWwub2ZmaWNlckBEU19kZW1vOmV5SmhiR2NpT2lKSVV6VXhNaUo5LmV5SmhkWFJvYjNKcGRIa2lPaUpQUmtaSlEwbEJUQ0lzSW5WelpYSkpaQ0k2SWxVdE1UVTVOalV4TlRFd016Y3lOaUlzSW5WelpYSlNaV1psY2lJNklrWXphR3RoTjJwS2JGTmFOMDVVUVRVelRXdHRNRUU5UFNJc0luVnpaWEp1WVcxbElqb2libTlrWVd3dWIyWm1hV05sY2tCRVUxOWtaVzF2SWl3aVkyVnVjM1Z6SWpvaU9UazVPVGs1SWl3aWJHOW5hVzVEYjNWdWRDSTZNVEkyTkN3aWFYTlFZWE56ZDI5eVpFTm9ZVzVuWldRaU9uUnlkV1VzSW14dloybHVWR2x0WlNJNk1UWXhPREl5TWpnM01EY3pOQ3dpWlhod2FYSjVJam96TUN3aWRtVnlhV1pwWTJGMGFXOXVJanA3SW1selQyWm1hV05wWVd3aU9tWmhiSE5sTENKcGMxWmxjbWxtYVdWa1JXMWhhV3dpT21aaGJITmxMQ0pwYzFabGNtbG1hV1ZrVFc5aWFXeGxJanAwY25WbExDSnBjMUpsWjJsemRISmhkR2x2YmtOdmJYQnNaWFJsWkNJNlptRnNjMlVzSW1selZtVnlhV1pwWldRaU9tWmhiSE5sZlN3aVlXTmpaWE56SWpwN0ltRmpZMlZ6YzBsa0lqb2lWVUV0TVRVNE5UazBOalF4TURRMU5TSXNJbUZqWTJWemMwNWhiV1VpT2lKVlRFSWdUbTlrWVd3Z1QyWm1hV05sY2lJc0ltRmpZMlZ6Y3lJNklsVk1RaUlzSW1GalkyVnpjMGhwWlhKaGNtTm9lVXhsZG1Wc0lqbzRMQ0prWlhCaGNuUnRaVzUwSWpwN0ltbGtJam9pTURreVlURmhZVFF0WWpFeE5DMDBOV1ppTFRrMk1qWXRaV1JoWWprek5XVXdaREpsSWl3aWJtRnRaU0k2SWtGa2JXbHVhWE4wY21GMGIzSWlmU3dpWkdWemFXZHVZWFJwYjI0aU9uc2lhV1FpT2lJeE5qSmpZelk0WkMwMk9HTXhMVFEzWVRRdE9ETmxNeTAzWldVM1lUZ3dOekk1WmpBaUxDSnVZVzFsSWpvaVRtOWtZV3dnVDJabWFXTmxjaUlzSW1selIyOTJRbTlrZVNJNmRISjFaWDBzSW1SbGMyTnlhWEIwYVc5dUlqb2lWV3hpSUc1dlpHRnNJRzltWm1salpYSWdkMmwwYUNCMWJHSWdZV05qWlhOeklpd2lkVzVwZEhNaU9sdGRMQ0p5YjJ4bElqb2lUMFpHU1VOSlFVd2lMQ0prWVhSbFEzSmxZWFJsWkNJNklqSXdNakF0TURRdE1ETlVNakE2TkRBNk1UQXVORFUzV2lKOUxDSmhZMk5sYzNOVGIzVnlZMlVpT25zaWFXUWlPaUpoWkRrMlkyTm1ZUzFoWlRrd0xUUTFNR1V0WVdFeFppMHdNakpoWkdNd09XUmtZalFpTENKd2JHRmpaVTVoYldVaU9pSkVaVzF2SUZWTVFpSXNJbk5vYjNKMFRtRnRaU0k2SWs1Qklpd2lZMjlrWlNJNklqazVPVGs1T1NKOUxDSndjbTltYVd4bElqcDdJbkJ5YjJacGJHVkpaQ0k2SWxBdE1UVTVOalV4TlRFd016Y3lOaUlzSW1WdFlXbHNJam9pYm1GcWFXSXVZV3RvZEdGeVFHbHVaR2xqYzI5bWRDNWpiMjBpTENKdGIySnBiR1VpT2lJNU9UVXpNREU1TXpBeklpd2ljR0Z1SWpvaUlpd2labWx5YzNST1lXMWxJam9pVG1GcWFXSWdRV3RvZEdGeUlpd2liR0Z6ZEU1aGJXVWlPaUlpTENKaFltOTFkQ0k2SWlJc0ltTnZiblJoWTNRaU9pSmhNRE0xTW1JNU15MDJabUl3TFRSaU1tWXRZV1V5WkMwMU5XSmhOVFJpWWpoallXWWlMQ0pwYzBGamRHbDJaU0k2ZEhKMVpYMHNJbWhwWlhKaGNtTm9lU0k2ZXlKemRHRjBaU0k2ZXlKcFpDSTZJalUzT1RZMU1tTTNMV1JpTldVdE5EQm1NeTA1T0RFeUxUVTVObVEyWVRKbVptRXlOU0lzSW01aGJXVWlPaUpFWlcxdklGTjBZWFJsSWl3aWMyaHZjblJPWVcxbElqb2lSRk1pTENKamIyUmxJam9pT1RraUxDSndjbWx0WVhKNVMyVjVJam94ZlN3aVpHbHpkSEpwWTNRaU9uc2lhV1FpT2lJMU16RTRNVGcyWkMwNVl6ZzBMVFJoWVRjdFlUSm1OUzAzWVRaallUTmlZbUU0WXpBaUxDSnVZVzFsSWpvaVJHVnRieUJFYVhOMGNtbGpkQ0lzSW5Ob2IzSjBUbUZ0WlNJNklrUkVWQ0lzSW1OdlpHVWlPaUk1T1RraUxDSndjbWx0WVhKNVMyVjVJam94ZlN3aWRXeGlJanA3SW1sa0lqb2lZV1E1Tm1OalptRXRZV1U1TUMwME5UQmxMV0ZoTVdZdE1ESXlZV1JqTURsa1pHSTBJaXdpYm1GdFpTSTZJa1JsYlc4Z1ZVeENJaXdpYzJodmNuUk9ZVzFsSWpvaVRrRWlMQ0pqYjJSbElqb2lPVGs1T1RrNUlpd2ljSEpwYldGeWVVdGxlU0k2TVgwc0luZGhjbVFpT25zaWFXUWlPaUlpTENKdVlXMWxJam9pSWl3aWMyaHZjblJPWVcxbElqb2lJaXdpWTI5a1pTSTZJaUlzSW5CeWFXMWhjbmxMWlhraU9qRjlMQ0poY21WaElqcDdJbWxrSWpvaUlpd2libUZ0WlNJNklpSXNJbk5vYjNKMFRtRnRaU0k2SWlJc0ltTnZaR1VpT2lJaUxDSndjbWx0WVhKNVMyVjVJam94ZlgxOS5XaERUYmdWUlliODJOQU1idGthQ0g1bWwyUVh3dHh1Z0pOMWFjYWUtNFRIOTExUG0zb01RSFNfRkpzbmYtVnBMV1dQNVlXZnV4NXMxZFJBOUw5OFJ5dw==")
                        .addHeader("Content-Type", "application/json")
                        .build();

                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull final Response response) throws IOException {
                        if (response.body() != null) {
                            String res = response.body().string();
                            runOnUiThread(() -> {
                                // Stuff that updates the UI
                                binding.tvResponse.setText(String.format("%s", res));
                            });
                        }
                    }
                });
            }
        });
    }
}